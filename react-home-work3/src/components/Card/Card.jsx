import React, { useState } from "react";
import "../Card/card.scss";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { FaStar } from "react-icons/fa";

function Card({
  getBasket,
  id,
  favouriteGet,
  img,
  title,
  color,
  price,
  children,
  removeCardBasket,
  isInBasket,
}) {
  const [isClicked, setIsClicked] = useState(false);
  const [isStarClicked, setIsStarClicked] = useState(false);
  const [modal, setModal] = useState(false);

  const clicked = () => {
    setIsClicked(!isClicked);
  };
  const modalTrue = () => {
    setModal(!modal);
  };
  const closeBtn = () => {
    setIsClicked(!isClicked);
  };
  const handleOk = () => {
    getBasket(id);
    closeBtn();
  };

  const reHandleOk = () => {
    removeCardBasket(id);
    closeBtn();
  };
  const handleAddToCart = () => {
    if (isInBasket) return;

    clicked();
  };

  const handelStar = () => {
    setIsStarClicked(true);
    favouriteGet(id);
  };
  const reHandelStar = () => {
    setIsStarClicked(false);
  };

  return (
    <>
      <div className="card-wrapper">
        <div className="card">
          <div onClick={modalTrue}>{children}</div>
          <div className="card__body">
            <img src={img} alt="" className="card__image" />
            <h2 className="card__title">{title}</h2>
            <span
              style={{ color }}
              onClick={!isStarClicked ? handelStar : reHandelStar}
              className="card__star"
            >
              <FaStar />
            </span>
            <p className="card__description">{price}&nbsp;$</p>
          </div>
          <Button
            text="Add to cart"
            onClick={handleAddToCart}
            className="card__btn"
          />
        </div>
      </div>
      {modal ? (
        <Modal
          onClicked={modalTrue}
          backgroundColorModal="#d50000"
          header="Do you want to remove a book from backet?"
          text="Are you sure"
          actions={[
            <Button key={1} onClick={reHandleOk} text="Ok" />,
            <Button key={2} onClick={modalTrue} text="Cancel" />,
          ]}
        />
      ) : null}
      {isClicked ? (
        <Modal
          onClicked={clicked}
          backgroundColorModal="grey"
          header="Do you want to add a book to your favorite?"
          text="Excellent choice"
          actions={[
            <Button key={1} onClick={handleOk} text="Ok" />,
            <Button key={2} onClick={clicked} text="Cancel" />,
          ]}
        />
      ) : null}
    </>
  );
}

export default Card;
