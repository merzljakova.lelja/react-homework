import React from "react";
import Card from "../../components/Card/Card";
const Home = ({ goods, favouriteGet, getBasket, removeCardBasket }) => {
  return (
    <>
      <div className="wrapper">
        {goods.map((elem) => {
          return (
            <Card
              key={elem.id}
              favouriteGet={favouriteGet}
              getBasket={getBasket}
              removeCardBasket={removeCardBasket}
              isInBasket={false}
              id={elem.id}
              img={elem.imgSrc}
              title={elem.title}
              price={elem.price}
              color={elem.color}
            />
          );
        })}
      </div>
    </>
  );
};

export default Home;
