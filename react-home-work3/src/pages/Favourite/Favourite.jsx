import React from "react";
import Card from "../../components/Card/Card";

const Favourite = ({ favourite, favouriteGet }) => {
  return (
    <>
      <div className="wrapper">
        {favourite.map((elem) => {
          return (
            <Card
              key={elem.id}
              favouriteGet={favouriteGet}
              id={elem.id}
              img={elem.imgSrc}
              title={elem.title}
              price={elem.price}
              color={elem.color}
            ></Card>
          );
        })}
      </div>
    </>
  );
};

export default Favourite;
