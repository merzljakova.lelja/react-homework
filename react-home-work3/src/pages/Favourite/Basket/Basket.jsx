import React from "react";
import Button from "../../../components/Button/Button";
import Card from "../../../components/Card/Card";

const Basket = ({ basket, getBasket, removeCardBasket }) => {
  return (
    <>
      <div className="wrapper">
        {basket.map((elem) => {
          return (
            <Card
              key={elem.id}
              getBasket={getBasket}
              removeCardBasket={removeCardBasket}
              id={elem.id}
              img={elem.imgSrc}
              title={elem.title}
              price={elem.price}
              isInBasket={true}
              color={elem.color}
            >
              <Button className="card__btn--close" text="X" />
            </Card>
          );
        })}
      </div>
    </>
  );
};

export default Basket;
