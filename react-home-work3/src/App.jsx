import "./App.Scss";
import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import Favourite from "./pages/Favourite/Favourite";
import Basket from "./pages/Favourite/Basket/Basket";
import Header from "./components/Header/Header";

function App() {
  const [goods, setGoods] = useState([]);
  const [favourite, setFavourite] = useState([]);
  const [basket, setBasket] = useState([]);

  useEffect(() => {
    if (localStorage.getItem("favourite")) {
      setFavourite(JSON.parse(localStorage.getItem("favourite")));
    }
    if (localStorage.getItem("basket")) {
      setBasket(JSON.parse(localStorage.getItem("basket")));
    }
    if (localStorage.getItem("goods")) {
      setGoods(JSON.parse(localStorage.getItem("goods")));
    } else {
      fetch("http://localhost:3000/goods.json")
        .then((res) => res.json())
        .then((result) => {
          setGoods(result.goods);
        });
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("favourite", JSON.stringify(favourite));
    localStorage.setItem("goods", JSON.stringify(goods));
    localStorage.setItem("basket", JSON.stringify(basket));
  }, [favourite, goods, basket]);

  const getBasket = (id) => {
    setBasket([goods.find((good) => good.id === id), ...basket]);
  };

  const removeCardBasket = (id) => {
    setBasket(
      basket.filter((e) => {
        return e.id !== id
      })
    );
  };

  const getFavourite = (id) => {
    const v = goods.find((good) => good.id === id);
    const filter = favourite.filter((elem) => {
      return elem.id !== v.id
    });
    setFavourite([goods.find((good) => good.id === id), ...filter]);
    setGoods([
      ...goods.map((item) => {
        if (item.id === id && item.color === "#222") {
          item.color = "red";
          return item;
        } else if (item.id === id && item.color === "red") {
          item.color = "#222";
          setFavourite(
            favourite.filter((e) => {
              return e.id !== item.id
            })
          );
          return item;
        }
        return item;
      }),
    ]);
  };

  return (
    <>
      <Header fav={favourite} bas={basket} />
      <div className="main">
        <Routes>
          <Route
            path="/"
            element={
              <Home
                favouriteGet={getFavourite}
                getBasket={getBasket}
                goods={goods}
                fav={favourite}
                bas={basket}
              />
            }
          />
          <Route
            path="/favourite"
            element={
              <Favourite favouriteGet={getFavourite} favourite={favourite} />
            }
          ></Route>
          <Route
            path="/basket"
            element={
              <Basket
                basket={basket}
                getBasket={getBasket}
                removeCardBasket={removeCardBasket}
              />
            }
          ></Route>
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </div>
    </>
  );
}

export default App;
