import "./App.Scss";
import React from "react";
import Card from "./components/Card/Card";



class App extends React.Component {
  state = {
    goods : [],
    favourite: [],
    basket : [],
  };
  componentDidMount() {
      if(localStorage.getItem("goods"))
      {
      this.setState({
      goods: JSON.parse(localStorage.getItem("goods"))
      })
      }else{
         fetch("http://localhost:3000/goods.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            goods: result.goods
          });
        }
      )
      }
      if(localStorage.getItem("favourite")){
         this.setState({
      favourite: JSON.parse(localStorage.getItem("favourite")),
      })
    }
      if(localStorage.getItem("basket")){
        this.setState({
          basket: JSON.parse(localStorage.getItem("basket")),
     })
      }   
  }
  componentDidUpdate(){
     localStorage.setItem("favourite", JSON.stringify(this.state.favourite))
     localStorage.setItem("goods", JSON.stringify(this.state.goods))
     localStorage.setItem("basket", JSON.stringify(this.state.basket))
  }
  getBasket = (id) => {
 this.setState({
  basket: [this.state.goods.find((good) => good.id === id) , ...this.state.basket]
 })
  }

  getFavourite = (id) =>{
      this.setState({
      favourite: [this.state.goods.find((good) => good.id === id) , ...this.state.favourite],
      goods: [...this.state.goods.map((item) => {
        if(item.id === id){
          item.color = "red"
          return item
        }
        return item
      })]
      })    
  }
    
   

  render() {
    const {favourite,basket,goods} = this.state
    return (
      <>
      <div style = {{display: "flex", paddingBottom: "15px", fontSize:"15px", fontStyle:"bolt", color:"black"}}>
        <img style = {{width:"2%",height:"2%"}} src="https://pngimg.com/uploads/heart/heart_PNG704.png"/>{favourite.length}
        </div>
      <div style = {{display: "flex", paddingBottom: "15px", fontSize:"15px", fontStyle:"bolt", color:"black"}}>
        <img style = {{width:"2%",height:"2%"}} src="https://webformyself.com/wp-content/uploads/2017/124/2.jpg"/>{basket.length}
        </div>
    <div className="wrapper">
      {goods.map((elem) =>{
        return <Card key={elem.id}
        favouriteGet={this.getFavourite}
        basketGet={this.getBasket}
        id={elem.id}
        img={elem.imgSrc}
        title={elem.title}
        price={elem.price}
        onClick={this.handleClick}
        color={elem.color}
      />
      } )}
    </div>
      </>
    );
  }
}

export default App;
