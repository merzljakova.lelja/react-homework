import React, { Component } from "react";
import "../Modal/modal.scss"

export default class Modal extends Component {
  render() {
    const {header,text,onClicked,actions,backgroundColorModal} = this.props
    return (
      <>
          <div onClick={onClicked} className="modal">
            <div onClick={(e) => e.stopPropagation()} className="modal__content" style={{ backgroundColor: backgroundColorModal}}>
              <div className="modal__header">
                <h2 className="modal__header-title">{header}</h2>
            <button className="modal__close" onClick={onClicked}>X</button>
            </div> 
            <p className="modal__body">{text}</p>
            <div className="modal__buttons">
            {actions}
            </div>
            </div>
          </div> 
      </>
    );
  }
}

