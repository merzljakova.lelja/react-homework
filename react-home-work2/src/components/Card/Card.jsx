import React, {Component} from "react";
import "../Card/card.scss";
import Button from "../Button/Button"
import Modal from "../Modal/Modal";
import { FaStar } from 'react-icons/fa';

class Card extends Component{
    state = {
        isClicked: false,
        isStarClicked: false,
    }
    myRef = React.createRef();

    clicked = () => {
        this.setState({
            isClicked : !this.state.isClicked,
        })
    }

    closeBtn = () =>{
      this.setState({
            isClicked : !this.state.isClicked,
        })
    }
    handleOk = () =>{
      this.props.basketGet(this.props.id)
      this.closeBtn()
    }
    
    handelStar = () =>{
       if(this.state.isStarClicked){
         this.setState({
        isStarClicked : true,
      })
    }
      this.props.favouriteGet(this.props.id)
      this.myRef.current.removeEventListener("click", this.handelStar)
    }

    componentDidMount() {
      this.myRef.current.addEventListener("click", this.handelStar);
    }
    
    componentWillUnmount() {
       this.myRef.current.removeEventListener("click", this.handelStar)
  }

    render(){
        const {isClicked} = this.state
        const {img, title, price,color} = this.props 
        return (
        <>
        <div className="card">
          <div className="card__body">
            <img src={img} className="card__image" />
            <h2 className="card__title">{title}</h2>
            <span style={{color}}  ref={this.myRef} className="card__star"><FaStar/></span>
            <p className="card__description">{price}&nbsp;$</p>
          </div>
          <Button text ="Add to cart" onClick={this.clicked} className="card__btn" />
        </div>
        {isClicked? <Modal onClicked={this.clicked} backgroundColorModal="grey" 
       header="You want to add a book to your favorite?"
       text ="Excellent choice"
       actions = {[<Button key={1} onClick = {this.handleOk} text="Ok" />, 
                  <Button key={2} onClick={this.clicked} text="Cancel"/>]} /> : null}
      </>
      )
    
    }
   
}

export default Card