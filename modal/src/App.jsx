import "./App.Scss";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends React.Component {
  state = {
    firstModal:  false,
    secondModal : false,
  };
  
  handleClick = () => {
   this.setState({
     firstModal: !this.state.firstModal,
   })
  };

  handleSecondClick = () => {
    this.setState({
      secondModal: !this.state.secondModal,
    })
   };
   

  render() {
    const {firstModal,secondModal} = this.state
    return (
      <>
      {firstModal? <Modal  backgroundColorModal = "#E94A35"
      text={['Once you delete this file, it won’t be possible to undo this action.',
             'Are you sure you want to delete it?']} 
      header='Do you want to delete this file?'  
      closeButton = {this.handleClick}
      actions={[<Button onClick={this.handleClick} text="Ok" backgroundColor="#801e11"/ >, 
                <Button onClick={this.handleClick} text="Cancel" backgroundColor="#801e11"/>]}
      /> 
               : <Button backgroundColor="green" text="Open first modal" onClick={this.handleClick} />}
      {secondModal? <Modal backgroundColorModal ="rgb(199, 125, 241)"
       text={['it’s not a good idea, maybe you need quite more time.','Are you sure ?']} 
       header='Do you want to delete this file?' 
       closeButton = {this.handleSecondClick} 
       actions={[<Button onClick={this.handleSecondClick} text="Ok" backgroundColor="rgb(54, 12, 78)"/ >, 
       <Button onClick={this.handleSecondClick} text="Cancel" backgroundColor="rgb(54, 12, 78)"/>]}
       />
        : <Button backgroundColor="blue" text="Open second modal" onClick={this.handleSecondClick} />}
      </>
    );
  }
}

export default App;
