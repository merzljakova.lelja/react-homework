import React, { Component } from 'react'

export default class Button extends Component {
    render() {
        const {text, onClick, backgroundColor} = this.props 
        return (
            <button style={{backgroundColor:backgroundColor, color:"#fff", marginLeft: "10px", width:"15%"}} 
            onClick = {onClick}>{text}</button>
        )
    }
}
